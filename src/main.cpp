#define _GNU_SOURCE
#include <iostream>
#include "Die.h"
#include "Dice.h"
#include "ScoreCard.h"
#include "game.cpp"


int main()
{       
    int category;
    char play = '0';
    game playGame;
    srand(time(nullptr));
    std::cout << "Press Enter to start the game...";
    std::cin.get(); // Wait for Enter key press
    do
    {
        playGame.Play();
        std::cout << "0 to play again" << endl;
        std::cin >> play;
    } while (play == '0');
}
