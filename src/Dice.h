#ifndef DICE_H
#define DICE_H
#include "Die.h"
#include <array>
#include <memory>
 using namespace std;

class Dice {
public:
    Dice() {
        for (auto& d : diceSet) {
            d = std::make_unique<Die>();  // Using smart pointers for better memory management
        }
    }

    std::array<int, 5> getNumbers() const {
        std::array<int, 5> temp;
        for (size_t i = 0; i < diceSet.size(); i++) {
            temp[i] = diceSet[i]->getNumber();
        }
        return temp;
    }

    void rollSet() {
        for (auto& die : diceSet) {
            die->roll();
        }
    }

    // Method to set dice values manually for testing
    void setDiceValues(const std::array<int, 5>& values) {
        for (size_t i = 0; i < diceSet.size(); i++) {
            diceSet[i]->setNumber(values[i]);
        }
    }

private:
    std::array<std::unique_ptr<Die>, 5> diceSet;
};

#endif // DICE_H
