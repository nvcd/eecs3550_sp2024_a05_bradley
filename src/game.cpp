#include <iostream>
#include "Die.h"
#include "Dice.h"
#include "ScoreCard.h"

using namespace std;
class game
{
public:
    game() {}

    void Play()
    {
        // Loop to take input and store it in the array
        /*
        std::cout << "Enter 5 numbers: ";
        for (int i = 0; i < 5; ++i) {
            std::cin >> numbers[i];
        }
        */

        int category;
        Dice rolledDice;
        ScoreCard score;        
        while (!score.scoreCardFull())
        {
        std::cout << "The die rolled ";
        rolledDice.rollSet();
        std::array<int, 5> numbers = rolledDice.getNumbers();
        for (int i = 0; i < 5; i++)
        {
            std::cout << numbers[i] << " ";
        }
            std::cout << endl;
            score.printPossibleScores(numbers, true);
            std::cout << "enter n for category you want" << endl;
            std::cin >> category;
            while (!score.setScore(numbers, category))
            {
                std::cout << "NOT VALID" << endl;
                std::cout << "enter n for category you want" << endl;
                std::cin >> category;
            }
            score.printScoreCard();
            std::cout << std::setw(18) << "total"
                      << " | " << score.getCurrentScore() << endl;
            if (score.scoreCardFull())
            { 
                return;
            }
        }
    }
};