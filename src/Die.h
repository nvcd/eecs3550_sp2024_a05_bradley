#ifndef DIE_H
#define DIE_H

class Die {
public:
    Die() : number((rand() % 6) + 1) {}

    int getNumber() const {
        return number;
    }

    void roll() {
        number = (rand() % 6) + 1;
    }

    // Setter to manually set the die number
    void setNumber(int newNumber) {
        if (newNumber >= 1 && newNumber <= 6) {
            number = newNumber;
        }
    }

private:
    int number;
};

#endif // DIE_H