#define CATCH_CONFIG_MAIN // This tells Catch to provide a main() - only do this in one cpp file
#include <catch2/catch.hpp>
#include "Dice.h" // Adjust this to the correct path or manage it via CMake include_directories
#include "ScoreCard.h"
// Tests 1-5
TEST_CASE("Dice values are set correctly", "[dice]")
{
    Dice dice;
    std::array<int, 5> testValues = {2, 3, 4, 5, 6};
    dice.setDiceValues(testValues);
    auto numbers = dice.getNumbers();

    REQUIRE(numbers.size() == 5);
    for (size_t i = 0; i < numbers.size(); i++)
    {
        REQUIRE(numbers[i] == testValues[i]);
    }
}

TEST_CASE("Scores a Yahtzee correctly", "[scorecard]")
{
    ScoreCard scoreCard1, scoreCard2;
    std::array<int, 5> testValues = {5, 5, 5, 5, 5};
    std::array<int, 5> testValues2 = {1, 5, 5, 5, 5};
    scoreCard1.setScore(testValues, 13);
    scoreCard1.setScore(testValues2, 13);

    REQUIRE(scoreCard1.getCurrentScore() == 50);
    REQUIRE(scoreCard2.getCurrentScore() == 0);
}

TEST_CASE("Scores the lowest possible game correctly", "[scorecard]")
{
    ScoreCard scoreCard1;
    std::array<int, 5> testValues = {5, 5, 5, 5, 5};
    scoreCard1.setScore(testValues, 1);
    scoreCard1.setScore(testValues, 2);
    scoreCard1.setScore(testValues, 3);
    scoreCard1.setScore(testValues, 4);
    scoreCard1.setScore(testValues, 6);
    testValues = {1, 2, 3, 3, 6};
    scoreCard1.setScore(testValues, 5);
    scoreCard1.setScore(testValues, 7);
    scoreCard1.setScore(testValues, 8);
    scoreCard1.setScore(testValues, 9);
    scoreCard1.setScore(testValues, 10);
    scoreCard1.setScore(testValues, 11);
    scoreCard1.setScore(testValues, 13);
    testValues = {1, 1, 1, 1, 1};
    scoreCard1.setScore(testValues, 12);

    REQUIRE(scoreCard1.getCurrentScore() == 5);
}

TEST_CASE("Scores the highest possible game correctly", "[scorecard]")
{
    ScoreCard scoreCard;
    std::array<int, 5> testValues = {1, 1, 1, 1, 1};
    scoreCard.setScore(testValues, 1);
    REQUIRE(scoreCard.getCurrentScore() == 5);

    testValues = {2, 2, 2, 2, 2};
    scoreCard.setScore(testValues, 2);
    REQUIRE(scoreCard.getCurrentScore() == 15);

    testValues = {3, 3, 3, 3, 3};
    scoreCard.setScore(testValues, 3);
    REQUIRE(scoreCard.getCurrentScore() == 30);

    testValues = {4, 4, 4, 4, 4};
    scoreCard.setScore(testValues, 4);
    REQUIRE(scoreCard.getCurrentScore() == 50);

    testValues = {5, 5, 5, 5, 5};
    scoreCard.setScore(testValues, 5);
    REQUIRE(scoreCard.getCurrentScore() == 75);

    testValues = {6, 6, 6, 6, 6};
    scoreCard.setScore(testValues, 6);
    REQUIRE(scoreCard.getCurrentScore() == 140);

    scoreCard.setScore(testValues, 7);
    REQUIRE(scoreCard.getCurrentScore() == 158);

    scoreCard.setScore(testValues, 8);
    REQUIRE(scoreCard.getCurrentScore() == 182);

    scoreCard.setScore(testValues, 12);
    REQUIRE(scoreCard.getCurrentScore() == 212);

    scoreCard.setScore(testValues, 13);
    REQUIRE(scoreCard.getCurrentScore() == 262);

    testValues = {1, 2, 3, 4, 5};
    scoreCard.setScore(testValues, 10);
    REQUIRE(scoreCard.getCurrentScore() == 292);

    scoreCard.setScore(testValues, 11);
    REQUIRE(scoreCard.getCurrentScore() == 332);

    testValues = {3, 6, 6, 6, 3};
    scoreCard.setScore(testValues, 9);
    REQUIRE(scoreCard.scoreCardFull() == true);
    REQUIRE(scoreCard.getCurrentScore() == 357);
}

TEST_CASE("Scores the same game the same scores", "[scorecard]")
{
    Dice dice;
    ScoreCard scoreCard1, scoreCard2;
    std::array<int, 5> testValue;
    for (int i = 1; i < 14; i++)
    {
        dice.rollSet();
        testValue = dice.getNumbers();
        scoreCard1.setScore(testValue, i);
        scoreCard2.setScore(testValue, i);
    }

    REQUIRE(scoreCard1.getCurrentScore() == scoreCard2.getCurrentScore());
}

// Tests 6-10
TEST_CASE("Scores a Chance correctly", "[scorecard]")
{
    Dice dice;
    ScoreCard scoreCard1, scoreCard2, scoreCard3;
    std::array<int, 5> testValues = {3, 4, 6, 6, 5};
    std::array<int, 5> testValues2 = {1, 3, 2, 4, 5};
    std::array<int, 5> testValues3 = {1, 1, 1, 1, 1};
    dice.setDiceValues(testValues);
    scoreCard1.setScore(testValues, 12);
    dice.setDiceValues(testValues2);
    scoreCard2.setScore(testValues2, 12);
    dice.setDiceValues(testValues3);
    scoreCard3.setScore(testValues3, 12);

    REQUIRE(scoreCard1.getCurrentScore() == 24);
    REQUIRE(scoreCard2.getCurrentScore() == 15);
    REQUIRE(scoreCard3.getCurrentScore() == 5);
}

TEST_CASE("Scores a Large Straight Correctly", "[scorecard]")
{
    Dice dice;
    ScoreCard scoreCard1, scoreCard2, scoreCard3, scoreCard4;
    std::array<int, 5> testValues = {1, 2, 3, 4, 5};
    std::array<int, 5> testValues2 = {2, 3, 4, 5, 6};
    std::array<int, 5> testValues3 = {3, 1, 4, 2, 5};
    std::array<int, 5> testValues4 = {3, 2, 4, 2, 5};
    dice.setDiceValues(testValues);
    scoreCard1.setScore(testValues, 11);
    dice.setDiceValues(testValues2);
    scoreCard2.setScore(testValues2, 11);
    dice.setDiceValues(testValues3);
    scoreCard3.setScore(testValues3, 11);
    dice.setDiceValues(testValues4);
    scoreCard4.setScore(testValues4, 11);

    REQUIRE(scoreCard1.getCurrentScore() == 40);
    REQUIRE(scoreCard2.getCurrentScore() == 40);
    REQUIRE(scoreCard3.getCurrentScore() == 40);
    REQUIRE(scoreCard4.getCurrentScore() == 0);
}

TEST_CASE("Scores a Short Straight Correctly", "[scorecard]")
{
    Dice dice;
    ScoreCard scoreCard1, scoreCard2, scoreCard3, scoreCard4, scoreCard5, scoreCard6;
    std::array<int, 5> testValues = {1, 2, 3, 4, 5};
    std::array<int, 5> testValues2 = {2, 3, 4, 5, 6};
    std::array<int, 5> testValues3 = {3, 1, 4, 2, 5};
    std::array<int, 5> testValues4 = {3, 2, 4, 2, 5};
    std::array<int, 5> testValues5 = {3, 2, 4, 4, 5};
    std::array<int, 5> testValues6 = {3, 4, 4, 1, 1};
    dice.setDiceValues(testValues);
    scoreCard1.setScore(testValues, 10);
    dice.setDiceValues(testValues2);
    scoreCard2.setScore(testValues2, 10);
    dice.setDiceValues(testValues3);
    scoreCard3.setScore(testValues3, 10);
    dice.setDiceValues(testValues4);
    scoreCard4.setScore(testValues4, 10);
    dice.setDiceValues(testValues5);
    scoreCard5.setScore(testValues5, 10);
    dice.setDiceValues(testValues6);
    scoreCard6.setScore(testValues6, 10);

    REQUIRE(scoreCard1.getCurrentScore() == 30);
    REQUIRE(scoreCard2.getCurrentScore() == 30);
    REQUIRE(scoreCard3.getCurrentScore() == 30);
    REQUIRE(scoreCard4.getCurrentScore() == 30);
    REQUIRE(scoreCard5.getCurrentScore() == 30);
    REQUIRE(scoreCard6.getCurrentScore() == 0);
}

TEST_CASE("Scores a Full House Correctly", "[scorecard]")
{
    Dice dice;
    ScoreCard scoreCard1, scoreCard2, scoreCard3, scoreCard4;
    std::array<int, 5> testValues = {1, 1, 3, 3, 3};
    std::array<int, 5> testValues2 = {3, 6, 6, 3, 6};
    std::array<int, 5> testValues3 = {3, 2, 2, 2, 3};
    std::array<int, 5> testValues4 = {3, 2, 4, 2, 5};
    dice.setDiceValues(testValues);
    scoreCard1.setScore(testValues, 9);
    dice.setDiceValues(testValues2);
    scoreCard2.setScore(testValues2, 9);
    dice.setDiceValues(testValues3);
    scoreCard3.setScore(testValues3, 9);
    dice.setDiceValues(testValues4);
    scoreCard4.setScore(testValues4, 9);

    REQUIRE(scoreCard1.getCurrentScore() == 25);
    REQUIRE(scoreCard2.getCurrentScore() == 25);
    REQUIRE(scoreCard3.getCurrentScore() == 25);
    REQUIRE(scoreCard4.getCurrentScore() == 0);
}

TEST_CASE("Scores a Four of a Kind Correctly", "[scorecard]")
{
    Dice dice;
    ScoreCard scoreCard1, scoreCard2, scoreCard3, scoreCard4;
    std::array<int, 5> testValues = {1, 3, 3, 3, 3};
    std::array<int, 5> testValues2 = {6, 6, 6, 3, 6};
    std::array<int, 5> testValues3 = {3, 2, 2, 2, 1};
    std::array<int, 5> testValues4 = {3, 2, 4, 2, 5};
    dice.setDiceValues(testValues);
    scoreCard1.setScore(testValues, 8);
    dice.setDiceValues(testValues2);
    scoreCard2.setScore(testValues2, 8);
    dice.setDiceValues(testValues3);
    scoreCard3.setScore(testValues3, 8);
    dice.setDiceValues(testValues4);
    scoreCard4.setScore(testValues4, 8);

    REQUIRE(scoreCard1.getCurrentScore() == 12);
    REQUIRE(scoreCard2.getCurrentScore() == 24);
    REQUIRE(scoreCard3.getCurrentScore() == 0);
    REQUIRE(scoreCard4.getCurrentScore() == 0);
}
// Tests 11-16
TEST_CASE("Calculates Sum Correctly", "[scorecard]")
{
    ScoreCard scoreCard;
    std::array<int, 5> oneValues = {1, 0, 0, 0, 0};
    std::array<int, 5> twoValues = {2, 0, 0, 0, 0};
    std::array<int, 5> threeValues = {3, 0, 0, 0, 0};
    std::array<int, 5> fourValues = {4, 0, 0, 0, 0};
    std::array<int, 5> fiveValues = {5, 0, 0, 0, 0};
    std::array<int, 5> sixValues = {6, 0, 0, 0, 0};

    int oneScore = 1;
    int twoScore = 2;
    int threeScore = 3;
    int fourScore = 4;
    int fiveScore = 5;
    int sixScore = 6;

    scoreCard.setScore(oneValues, 1);
    scoreCard.setScore(twoValues, 2);
    scoreCard.setScore(threeValues, 3);
    scoreCard.setScore(fourValues, 4);
    scoreCard.setScore(fiveValues, 5);
    scoreCard.setScore(sixValues, 6);

    REQUIRE(scoreCard.getCurrentScore() == 21);
}
TEST_CASE("Calculates Bonus Correctly", "[scorecard]")
{
    // test with bonus
    Dice dice;
    ScoreCard scoreCardB;
    std::array<int, 5> oneValuesB = {1, 1, 1, 1, 0};
    std::array<int, 5> twoValuesB = {2, 2, 2, 2, 0};
    std::array<int, 5> threeValuesB = {3, 3, 3, 3, 0};
    std::array<int, 5> fourValuesB = {4, 4, 4, 4, 0};
    std::array<int, 5> fiveValuesB = {5, 5, 5, 5, 0};
    std::array<int, 5> sixValuesB = {6, 6, 6, 6, 0};

    int oneScoreB = 4;
    int twoScoreB = 8;
    int threeScoreB = 12;
    int fourScoreB = 16;
    int fiveScoreB = 20;
    int sixScoreB = 24;

    scoreCardB.setScore(oneValuesB, 1);
    scoreCardB.setScore(twoValuesB, 2);
    scoreCardB.setScore(threeValuesB, 3);
    scoreCardB.setScore(fourValuesB, 4);
    scoreCardB.setScore(fiveValuesB, 5);
    scoreCardB.setScore(sixValuesB, 6);
    int total = oneScoreB + twoScoreB + threeScoreB + fourScoreB + fiveScoreB + sixScoreB;

    // test without bonus
    REQUIRE(scoreCardB.getCurrentScore() - 35 == total);
}

TEST_CASE("Test scoring 1", "[scorecard]")
{
    Dice dice;
    ScoreCard scoreCard1, scoreCard2, scoreCard3, scoreCard4, scoreCard5, scoreCard6;

    std::array<int, 5> testValues1 = {1, 1, 1, 1, 2};
    std::array<int, 5> testValues2 = {1, 1, 1, 2, 2};
    std::array<int, 5> testValues3 = {1, 1, 2, 2, 2};
    std::array<int, 5> testValues4 = {1, 2, 2, 2, 2};
    std::array<int, 5> testValues5 = {2, 2, 2, 2, 2};
    std::array<int, 5> testValues6 = {1, 1, 1, 1, 1};

    scoreCard1.setScore(testValues1, 1);
    scoreCard2.setScore(testValues2, 1);
    scoreCard3.setScore(testValues3, 1);
    scoreCard4.setScore(testValues4, 1);
    scoreCard5.setScore(testValues5, 1);
    scoreCard6.setScore(testValues6, 1);

    REQUIRE(scoreCard1.getCurrentScore() == 4);
    REQUIRE(scoreCard2.getCurrentScore() == 3);
    REQUIRE(scoreCard3.getCurrentScore() == 2);
    REQUIRE(scoreCard4.getCurrentScore() == 1);
    REQUIRE(scoreCard5.getCurrentScore() == 0);
    REQUIRE(scoreCard6.getCurrentScore() == 5);
}

TEST_CASE("Doesn't Allow Repeat Scoring", "[scorecard]")
{
    Dice dice;
    ScoreCard scoreCard;
    std::array<int, 5> testValues1 = {1, 1, 1, 1, 2};
    std::array<int, 5> testValues2 = {1, 1, 1, 2, 2};
    std::array<int, 5> testValues3 = {1, 1, 2, 2, 2};
    std::array<int, 5> testValues4 = {1, 2, 2, 2, 2};
    std::array<int, 5> testValues5 = {2, 2, 2, 2, 2};
    std::array<int, 5> testValues6 = {1, 1, 1, 1, 1};

    scoreCard.setScore(testValues1, 1);
    scoreCard.setScore(testValues2, 1);
    scoreCard.setScore(testValues3, 1);
    scoreCard.setScore(testValues4, 1);
    scoreCard.setScore(testValues5, 1);
    scoreCard.setScore(testValues6, 1);

    REQUIRE(scoreCard.getCurrentScore() == 4);
}

TEST_CASE("Lets you play 4 of a kind as 3 of a kind", "[scorecard]")
{
    Dice dice;
    ScoreCard scoreCard;
    std::array<int, 5> testValues1 = {1, 1, 1, 1, 2};

    scoreCard.setScore(testValues1, 7);

    REQUIRE(scoreCard.getCurrentScore() == 3);
}

// Tests 16-20

TEST_CASE("Test scoring 2", "[scorecard]")
{
    Dice dice;
    ScoreCard scoreCard1, scoreCard2, scoreCard3, scoreCard4, scoreCard5;
    std::array<int, 5> testValues1 = {1, 1, 1, 1, 2};
    std::array<int, 5> testValues2 = {1, 1, 1, 2, 2};
    std::array<int, 5> testValues3 = {1, 1, 2, 2, 2};
    std::array<int, 5> testValues4 = {1, 2, 2, 2, 2};
    std::array<int, 5> testValues5 = {2, 2, 2, 2, 2};

    scoreCard1.setScore(testValues1, 2);
    scoreCard2.setScore(testValues2, 2);
    scoreCard3.setScore(testValues3, 2);
    scoreCard4.setScore(testValues4, 2);
    scoreCard5.setScore(testValues5, 2);

    REQUIRE(scoreCard1.getCurrentScore() == 2);
    REQUIRE(scoreCard2.getCurrentScore() == 4);
    REQUIRE(scoreCard3.getCurrentScore() == 6);
    REQUIRE(scoreCard4.getCurrentScore() == 8);
    REQUIRE(scoreCard5.getCurrentScore() == 10);
}

TEST_CASE("Test scoring 3", "[scorecard]")
{
    Dice dice;
    ScoreCard scoreCard1, scoreCard2, scoreCard3, scoreCard4, scoreCard5;
    std::array<int, 5> testValues1 = {1, 1, 1, 1, 3};
    std::array<int, 5> testValues2 = {1, 1, 1, 3, 3};
    std::array<int, 5> testValues3 = {1, 1, 3, 3, 3};
    std::array<int, 5> testValues4 = {1, 3, 3, 3, 3};
    std::array<int, 5> testValues5 = {3, 3, 3, 3, 3};

    scoreCard1.setScore(testValues1, 3);
    scoreCard2.setScore(testValues2, 3);
    scoreCard3.setScore(testValues3, 3);
    scoreCard4.setScore(testValues4, 3);
    scoreCard5.setScore(testValues5, 3);

    REQUIRE(scoreCard1.getCurrentScore() == 3);
    REQUIRE(scoreCard2.getCurrentScore() == 6);
    REQUIRE(scoreCard3.getCurrentScore() == 9);
    REQUIRE(scoreCard4.getCurrentScore() == 12);
    REQUIRE(scoreCard5.getCurrentScore() == 15);
}

TEST_CASE("Test scoring 4", "[scorecard]")
{
    Dice dice;
    ScoreCard scoreCard1, scoreCard2, scoreCard3, scoreCard4, scoreCard5;
    std::array<int, 5> testValues1 = {1, 1, 1, 1, 4};
    std::array<int, 5> testValues2 = {1, 1, 1, 4, 4};
    std::array<int, 5> testValues3 = {1, 1, 4, 4, 4};
    std::array<int, 5> testValues4 = {1, 4, 4, 4, 4};
    std::array<int, 5> testValues5 = {4, 4, 4, 4, 4};

    scoreCard1.setScore(testValues1, 4);
    scoreCard2.setScore(testValues2, 4);
    scoreCard3.setScore(testValues3, 4);
    scoreCard4.setScore(testValues4, 4);
    scoreCard5.setScore(testValues5, 4);

    REQUIRE(scoreCard1.getCurrentScore() == 4);
    REQUIRE(scoreCard2.getCurrentScore() == 8);
    REQUIRE(scoreCard3.getCurrentScore() == 12);
    REQUIRE(scoreCard4.getCurrentScore() == 16);
    REQUIRE(scoreCard5.getCurrentScore() == 20);
}

TEST_CASE("Test scoring 5", "[scorecard]")
{
    Dice dice;
    ScoreCard scoreCard1, scoreCard2, scoreCard3, scoreCard4, scoreCard5;
    std::array<int, 5> testValues1 = {1, 1, 1, 1, 5};
    std::array<int, 5> testValues2 = {1, 1, 1, 5, 5};
    std::array<int, 5> testValues3 = {1, 1, 5, 5, 5};
    std::array<int, 5> testValues4 = {1, 5, 5, 5, 5};
    std::array<int, 5> testValues5 = {5, 5, 5, 5, 5};

    scoreCard1.setScore(testValues1, 5);
    scoreCard2.setScore(testValues2, 5);
    scoreCard3.setScore(testValues3, 5);
    scoreCard4.setScore(testValues4, 5);
    scoreCard5.setScore(testValues5, 5);

    REQUIRE(scoreCard1.getCurrentScore() == 5);
    REQUIRE(scoreCard2.getCurrentScore() == 10);
    REQUIRE(scoreCard3.getCurrentScore() == 15);
    REQUIRE(scoreCard4.getCurrentScore() == 20);
    REQUIRE(scoreCard5.getCurrentScore() == 25);
}

TEST_CASE("Test scoring 6", "[scorecard]")
{
    Dice dice;
    ScoreCard scoreCard1, scoreCard2, scoreCard3, scoreCard4, scoreCard5;
    std::array<int, 5> testValues1 = {1, 1, 1, 1, 6};
    std::array<int, 5> testValues2 = {1, 1, 1, 6, 6};
    std::array<int, 5> testValues3 = {1, 1, 6, 6, 6};
    std::array<int, 5> testValues4 = {1, 6, 6, 6, 6};
    std::array<int, 5> testValues5 = {6, 6, 6, 6, 6};

    scoreCard1.setScore(testValues1, 6);
    scoreCard2.setScore(testValues2, 6);
    scoreCard3.setScore(testValues3, 6);
    scoreCard4.setScore(testValues4, 6);
    scoreCard5.setScore(testValues5, 6);

    REQUIRE(scoreCard1.getCurrentScore() == 6);
    REQUIRE(scoreCard2.getCurrentScore() == 12);
    REQUIRE(scoreCard3.getCurrentScore() == 18);
    REQUIRE(scoreCard4.getCurrentScore() == 24);
    REQUIRE(scoreCard5.getCurrentScore() == 30);
}